import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'primo';
  controllo?: boolean=true; // ? 
  myVar: string="A";
  choice: number=0;
  items : string[]=["primo","socondo","terzo"]

  people = [
     { name: 'Anderson', age: 35, city: 'Sao Paulo' },
     { name: 'John', age: 12, city: 'Miami' },
     { name: 'Peter', age: 22, city: 'New York' }
     ];

  stile:any = {
    'color': 'yellow', 
    'background-color': 'blue',
    'font-size':'30px'
  };

  visualizzaOnOff(){
    this.controllo = ! this.controllo;
  }

  cambiaScelta(){
    this.choice++;
    this.stile['font-size']=this.choice*10 + "px";
  }

  scegli(num:number,item:any){
    console.warn(num);
    console.warn(item);
  }
}
